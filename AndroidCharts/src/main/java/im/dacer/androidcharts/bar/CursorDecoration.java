package im.dacer.androidcharts.bar;

import android.content.Context;
import android.graphics.Canvas;
import android.view.View;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class CursorDecoration extends RecyclerView.ItemDecoration {

    private final SingleBarContext c;
    private final Context context;
    private final LinearLayoutManager layoutManager;
    private int where;
    private float offset;

    private boolean visible = false;

    public CursorDecoration(SingleBarContext barContext, Context context, LinearLayoutManager layoutManager) {
        c = barContext;
        this.context = context;
        this.layoutManager = layoutManager;
    }

    @Override
    public void onDrawOver(@NonNull Canvas c, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        super.onDrawOver(c, parent, state);

        if (visible) {

            View selectedView = layoutManager.findViewByPosition(where);

            if (selectedView != null)
                c.drawRect(selectedView.getX() + offset100() * offset,
                        selectedView.getY() + selectedView.getHeight(),
                        selectedView.getX() + selectedView.getWidth() + offset100() * offset,
                        selectedView.getY() + this.c.linePaint.getStrokeWidth(), this.c.linePaint
                );

        }
    }

    /**
     * Pixel value by which an offset value of 100% would move the cursor.
     */
    private int offset100() {
        return c.barSideMargin + c.barWidth;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public void setScrollPosition(int position, float offset) {
        where = position;
        this.offset = offset;
    }
}
