package im.dacer.androidcharts.bar;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.*;
import android.graphics.drawable.Drawable;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import im.dacer.androidcharts.MyUtils;
import im.dacer.androidcharts.R;

public class FogOfWarDecoration extends RecyclerView.ItemDecoration {

    private static final int[] ATTRS = new int[]{ android.R.attr.windowBackground };

    private final Context context;
    private final Side side;


    private boolean visible;

    FogOfWarDecoration(Context context, Side side) {
        this.context = context;
        this.side = side;
    }

    @Override
    public void onDrawOver(@NonNull Canvas c, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        super.onDrawOver(c, parent, state);

        if (visible) {
            int width = MyUtils.dip2px(context, 8);
            int height = parent.getHeight();
            int left;
            if (side == Side.RIGHT) {
                left = parent.getWidth() - width;
            } else {
                left = parent.getPaddingLeft() - 1;
            }

            final TypedArray a = context.obtainStyledAttributes(ATTRS);
            Drawable windowBackgroundDrawable = a.getDrawable(0);
            if (windowBackgroundDrawable == null) {
                Log.w(FogOfWarDecoration.class.getSimpleName(), "@android:attr/windowBackground was not set in the theme. Fog of war cannot be drawn.");
                return;
            } else {
                windowBackgroundDrawable = windowBackgroundDrawable.mutate();
            }
            a.recycle();

            // Draw window background as bitmap
            Bitmap windowBackgroundBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            Canvas windowBackgroundCanvas = new Canvas(windowBackgroundBitmap);
            windowBackgroundDrawable.setBounds(0, 0, width, height);
            windowBackgroundDrawable.draw(windowBackgroundCanvas);

            // Draw transparency gradient as bitmap
            Bitmap transparencyGradientBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            Canvas transparencyGradientCanvas = new Canvas(transparencyGradientBitmap);
            Drawable transparencyGradientDrawable = context.getDrawable(R.drawable.transparency_gradient);
            transparencyGradientDrawable.setBounds(0, 0, width, height);
            transparencyGradientDrawable.draw(transparencyGradientCanvas);

            // Apply transparency gradient to window background
            Paint paint = new Paint();
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));

            Matrix flipHorizontalMatrix = new Matrix();
            if (side == Side.LEFT) {
                flipHorizontalMatrix.setScale(-1, 1);
                flipHorizontalMatrix.postTranslate(transparencyGradientBitmap.getWidth(), 0);
            }
            windowBackgroundCanvas.drawBitmap(transparencyGradientBitmap, flipHorizontalMatrix, paint);

            c.drawBitmap(windowBackgroundBitmap, left, 0, null);
        }
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    enum Side { LEFT, RIGHT }
}
