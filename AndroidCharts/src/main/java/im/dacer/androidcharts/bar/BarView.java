package im.dacer.androidcharts.bar;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

public class BarView extends FrameLayout {

    protected RecyclerView recycler;
    private Adapter adapter;
    private LegendView legend;
    protected SingleBarContext barContext;

    protected LabelItemDecoration labelItemDecoration;

    protected FogOfWarDecoration fogOfWarLeft, fogOfWarRight;

    protected CursorDecoration cursorDecoration;

    private SpaceItemDecoration spaceDecoration;

    private OnBarClickListener onClickListener;

    private final Runnable animator = new Runnable() {
        @Override
        public void run() {
            if (adapter.animationStep(recycler)) {
                postDelayed(this, 20);
            }
        }
    };

    private final RecyclerView.OnScrollListener horizontalLineScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
            int x = recyclerView.computeHorizontalScrollOffset();
            legend.setScrolledX(x);
        }
    };

    private final RecyclerView.OnScrollListener fogOfWarScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
            fogOfWarRight.setVisible(recyclerView.canScrollHorizontally(+1));
            fogOfWarLeft.setVisible(recyclerView.canScrollHorizontally(-1));
        }
    };

    public BarView(Context context) {
        super(context);
        init();
    }

    public BarView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BarView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        barContext = getBarContext();
        legend = addLegend();
        recycler = addRecycler();
        legend.attachRecycler(recycler);

    }

    protected SingleBarContext getBarContext() {
        return new SingleBarContext(getContext());
    }

    protected LabelItemDecoration<? extends SingleBarContext> getLabelItemDecoration(SingleBarContext barContext) {
        return new BarLabelItemDecoration(barContext);
    }

    private LegendView addLegend() {
        LegendView legend = new LegendView(getContext(), barContext);
        addView(legend);
        return legend;
    }

    private RecyclerView addRecycler() {
        RecyclerView recycler = new RecyclerView(getContext());
        final LinearLayoutManager manager;
        recycler.setLayoutManager(manager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));

        recycler.addItemDecoration(spaceDecoration = new SpaceItemDecoration(barContext.barSideMargin));
        recycler.addItemDecoration(labelItemDecoration = getLabelItemDecoration(barContext));
        recycler.addItemDecoration(cursorDecoration = new CursorDecoration(barContext, getContext(), manager));
        recycler.addItemDecoration(fogOfWarLeft = new FogOfWarDecoration(getContext(), FogOfWarDecoration.Side.LEFT));
        recycler.addItemDecoration(fogOfWarRight = new FogOfWarDecoration(getContext(), FogOfWarDecoration.Side.RIGHT));

        recycler.addOnScrollListener(fogOfWarScrollListener);

        recycler.setAdapter(adapter = new Adapter(barContext, new OnClickListener() {
            @Override
            public void onClick(View view) {
                int position = manager.getPosition(view);
                onClickListener.onClick(position);
            }
        }));
        recycler.setPadding(0, barContext.topMargin, 0, 0);

        recycler.setItemViewCacheSize(10);

        addView(recycler);
        return recycler;
    }

    public final void setData(Value[] values) {
        setData(values, 0);
    }

    /**
     * @param max The top border of the chart, or 0 to use highest value
     */
    public final void setData(Value[] values, int max) {

        int highestValue = Collections.max(Arrays.asList(values), new Comparator<Value>() {
            @Override
            public int compare(Value x, Value y) {
                return (x.getValue() < y.getValue()) ? -1 : ((x.getValue() == y.getValue()) ? 0 : 1);
            }
        }).getValue();

        if (max < highestValue) {
            if (max != 0) {
                Log.w("BarView", "Inappropriate max! Using highest value as max instead. If you meant to do that, pass 0 to remove this warning.");
            }
            max = highestValue;
        }

        adapter.setData(values, max);

        labelItemDecoration.setValues(values);

        recycler.invalidateItemDecorations();

        // Stop ongoing animation
        removeCallbacks(animator);

        // Start new animation
        post(animator);
    }

    public void setHorizontalLines(Line[] lines, int max) {
        legend.setLines(lines, max);
        recycler.setPadding(barContext.lineLabelWidth + 2 * barContext.textMargin, barContext.topMargin, 0, 0);
    }

    public void setZeroLineEnabled(boolean enabled) {
        recycler.removeItemDecoration(spaceDecoration);

        recycler.addItemDecoration(
                spaceDecoration = enabled?
                        new SpaceItemDecoration(barContext.barSideMargin)
                        : new SpaceItemDecoration.ZeroLineDecoration(barContext)
        );
    }

    /**
     * Set to <code>true</code> if the horizontal line should scroll along. This causes
     * performance issues if a transparent background color is passed, and draws incorrect
     * colors if an incorrect background color is passed. <code>backgroundColor</code>
     * is ignored if <code>scroll</code> is false.
     */
    public void setScrollHorizontalLines(boolean scroll, @ColorInt int backgroundColor) {
        if (scroll) {
            // Attach legend to recycler view
            recycler.addOnScrollListener(horizontalLineScrollListener);
            legend.attachBackgroundColor(backgroundColor);
        } else {
            recycler.removeOnScrollListener(horizontalLineScrollListener);
            legend.attachBackgroundColor(0x00000000);
        }

    }

    /**
     * "Fog of war" refers to the scroll indicator shown at the edges of the bar chart. It
     * uses the window background attribute from the theme, which may lead to unexpected
     * results. It is enabled by default.
     */
    public void setShowFogOfWar(boolean show) {
        if (show) {
            recycler.addOnScrollListener(fogOfWarScrollListener);
        } else {
            recycler.removeOnScrollListener(fogOfWarScrollListener);
            fogOfWarLeft.setVisible(false);
            fogOfWarRight.setVisible(false);
        }
        recycler.invalidateItemDecorations();
    }

    /**
     * If you want to show a cursor, enable it with this method and consequently call
     * {@link #setCursorPosition(int, float)}.
     */
    public void setShowCursor(boolean show) {
        cursorDecoration.setVisible(show);
        recycler.invalidateItemDecorations();
    }

    public void setCursorPosition(int position, float positionOffset) {
        cursorDecoration.setScrollPosition(position, positionOffset);
        recycler.invalidateItemDecorations();

        // Calculate range that should be visible
        float fractionalPosition = position + positionOffset;
        int lower = (int) Math.max(0, (fractionalPosition - 1.5) * barContext.barWidth + (fractionalPosition - 1) * barContext.barSideMargin);
        int upper = (int) ((fractionalPosition + 2.5) * barContext.barWidth + (fractionalPosition + 3) * barContext.barSideMargin);

        if (upper - lower > recycler.computeHorizontalScrollExtent()) {
            // Formula doesn't work for these parameters
            // two margins left of the current cursor position (the first one is the "missing" padding to the left that we are omitting)
            lower = (int) (fractionalPosition * (barContext.barSideMargin + barContext.barWidth)) - barContext.barSideMargin;
            // two margins right of the current cursor position
            upper = lower + barContext.barWidth + barContext.barSideMargin * 4;

            if (upper - lower > recycler.computeHorizontalScrollExtent()) {
                // We probably have very wide bars. Scroll so that middle of bar is visible.
                lower = upper = (int) (barContext.barSideMargin + (fractionalPosition + 0.5) * (barContext.barSideMargin + barContext.barWidth));

            }
        }

        // Calculate actually visible range
        int leftX = recycler.computeHorizontalScrollOffset();
        // Left X is underestimated if first item is not visible
        if (((LinearLayoutManager) recycler.getLayoutManager()).findFirstVisibleItemPosition() > 0) {
            leftX += barContext.barSideMargin;
        }

        int rightX = leftX + recycler.computeHorizontalScrollExtent();

        // Scroll to match the desired range
        if (leftX > lower) {
            recycler.scrollBy(lower - leftX, 0);
        } else if (rightX < upper) {
            recycler.scrollBy(upper - rightX, 0);
        }

    }

    public void setOnBarClickListener(OnBarClickListener onClickListener) {
        barContext.selectable = onClickListener != null;
        LinearLayoutManager manager = (LinearLayoutManager) recycler.getLayoutManager();
        for (int i = 0; i < manager.getChildCount(); i++) {
            SingleBarView view = (SingleBarView) manager.getChildAt(i);
            view.setEnabled(onClickListener != null);
        }
        this.onClickListener = onClickListener;
    }

    public void scrollToEnd() {
        recycler.scrollToPosition(adapter.getItemCount() - 1);
    }

    public void scrollTo(int position, boolean smooth) {
        position = Math.max(0, Math.min(position, adapter.getItemCount()));
        if (smooth) recycler.smoothScrollToPosition(position);
        else recycler.scrollToPosition(position);
    }

    public interface OnBarClickListener {
        void onClick(int position);
    }
}
