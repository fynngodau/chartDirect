package com.dacer.androidchartsexample;

import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;
import godau.fynn.typedrecyclerview.PrimitiveViewHolder;
import im.dacer.androidcharts.bar.BarView;
import im.dacer.androidcharts.bar.Line;
import im.dacer.androidcharts.bar.MultiValue;
import im.dacer.androidcharts.bar.Value;

/**
 * Created by Dacer on 11/15/13.
 */
public class CursorFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_bar_cursor, container, false);
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final BarView barView = view.findViewById(R.id.bar_view);
        barView.setShowCursor(true);
        randomSet(barView);


        final ViewPager2 viewPager = view.findViewById(R.id.view_pager);
        viewPager.setAdapter(new RecyclerView.Adapter<PrimitiveViewHolder<TextView>>() {
            @NonNull
            @Override
            public PrimitiveViewHolder<TextView> onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                return new PrimitiveViewHolder(LayoutInflater.from(getActivity()).inflate(R.layout.fragment_bar_cursor_page, parent, false));
            }

            @Override
            public void onBindViewHolder(@NonNull PrimitiveViewHolder<TextView> holder, int position) {
                holder.itemView.setText(Data.values()[position].name());
            }

            @Override
            public int getItemCount() {
                return Data.values().length;
            }
        });

        viewPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                barView.setCursorPosition(position, positionOffset);
            }
        });

        barView.setOnBarClickListener(new BarView.OnBarClickListener() {
            @Override
            public void onClick(int position) {
                viewPager.setCurrentItem(position, true);
            }
        });

        final Button toggle = view.findViewById(R.id.toggle);
        toggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setVisibility(View.GONE - viewPager.getVisibility());
            }
        });
    }

    protected void randomSet(BarView barView) {

        int amount = Data.values().length;

        Value[] values = new Value[amount];
        for (int i = 0; i < amount; i += 2) {
            values[i] = new Value((int) (Math.random() * 100), String.valueOf(i + 1));
        }
        for (int i = 1; i < amount; i += 2) {
            values[i] = new MultiValue(
                    new float[]{0.5f, 0.5f},
                    (int) (Math.random() * 100),
                    new Integer[]{Color.YELLOW, Color.BLUE},
                    String.valueOf(i + 1)
            );
        }
        barView.setData(values, 100);
    }

    enum Data {
        ENTRY_1, ENTRY_2, ENTRY_3, ENTRY_4, ENTRY_5, ENTRY_6, MORE_ENTRIES, YET_ANOTHER_ENTRY, NEED, MORE, ENTRIES,
        WRITE, TEST, DATA, YET, ANOTHER, ROW, OF, ENUM_ENTRIES
    }
}